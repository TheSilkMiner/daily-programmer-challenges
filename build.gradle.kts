buildscript {
	repositories {
		mavenCentral()
		jcenter()
        google()
	}
}

plugins {
    java
    kotlin("jvm") version "1.3.11"
    id("idea")
    id("net.minecrell.licenser") version "0.4.1"
}

license {
    header = project.file("LICENSE")

    include("**/*.java")
    include("**/*.kt")

    ignoreFailures = false
}

repositories {
	mavenCentral()
	jcenter()
    google()
}

dependencies {
    // Kotlin Standard Lib
    compile(kotlin("stdlib"))
    
    // ======== TEST ONLY DEPENDENCIES ======== //

	// JUnit
	testCompile(group = "junit", name = "junit", version = "4.12")
    
    // Kotlin JUnit
    testCompile(group = "org.jetbrains.kotlin", name = "kotlin-test-junit", version = "1.3.11")
}

tasks.withType<Wrapper> {
	gradleVersion = "5.0"
    distributionType = Wrapper.DistributionType.ALL
}
