package net.thesilkminer.challenges

private fun String.checkUpcLength() = if (this.length != 11) throw IllegalArgumentException("UPC must be 11 digits long") else this
private fun String.toDigitList() = this.asSequence().map { it.toString().toInt() }.toList()
private fun Int.isEven() = this % 2 == 0

fun upc(upc: Long) = upc(upc.toString(10).padStart(11, '0'))
fun upc(upc: String) = upc.checkUpcLength()
        .toDigitList()
        .reduceIndexed { index, sum, i -> sum + if (index.isEven()) i else 0 }
        .times(3)
        .plus(upc.toDigitList().subList(1, upc.toDigitList().size).reduceIndexed { index, sum, i -> sum + if (index.isEven()) i else 0 })
        .rem(10)
        .run { if (this == 0) 0 else 10 - this }

