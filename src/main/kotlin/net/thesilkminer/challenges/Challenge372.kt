@file:JvmName("372")

package net.thesilkminer.challenges

private const val SURELY_BALANCED_STRING = "xy"
private const val XY_BUT_ONLY_XY = "^[xy]+$"

private fun t(): Nothing = throw IllegalArgumentException("This type of string is supported only in bonus mode")
private fun String.h() = this.matches(Regex(XY_BUT_ONLY_XY))
private fun String.validateString(b: Boolean) = if (b || (!b && this.h())) { this } else { t() }
private fun <K, V> Map<K, V>.checkCount(b: Boolean) = if (b || this.entries.count() == 2) { this } else { mapOf() }

fun balanced(string: String, isBonus: Boolean = false) = string
        .ifEmpty { SURELY_BALANCED_STRING } // Run through an already balanced string if the current one is empty
        .validateString(isBonus)
        .asSequence()
        .groupingBy { it }
        .eachCount()
        .checkCount(isBonus)
        .entries
        .map { it.value }
        .distinct()
        .count() == 1

@Suppress("FunctionName") fun balanced_bonus(string: String) = balanced(string, true)
