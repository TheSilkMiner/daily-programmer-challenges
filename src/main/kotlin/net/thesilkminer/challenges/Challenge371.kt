@file:JvmName("371")

package net.thesilkminer.challenges

// Here we use a var so we can exploit mutability in the Pair
// instead of having to copy the object every time. In any other
// cases I'd strongly suggest to make this class immutable.
private data class TriState(var state: TriState.State = TriState.State.NOT_FOUND) {
    enum class State { NOT_FOUND, ONE, MORE }
}

private data class BoardIndex(val col: Int, val row: Int)

private class Board(val size: Int) {
    // columns -> rows -> cells
    private val board: Array<Array<Boolean>> = Array(this.size) { Array(this.size) { false } }

    fun placeQueenAt(index: BoardIndex) {
        this.board[index.col][index.row] = true
    }
    fun hasQueenAt(index: BoardIndex) = this.board[index.col][index.row]

    // Returns the index of the columns (0-based) where queens are in the wrong positions
    fun findMisplacedQueens(): Set<Int> {
        val list = mutableSetOf<Int>()
        list.addAll(this.checkRows())
        list.addAll(this.checkDiagonals())
        return list
    }

    private fun checkRows(): Set<Int> {
        val misplacedQueens = mutableSetOf<Int>()

        for (j in 0 until this.size) {
            var found = false
            for (i in 0 until this.size) {
                if (this.hasQueenAt(BoardIndex(i, j))) {
                    if (found) {
                        misplacedQueens.add(i)
                        continue
                    }
                    found = true
                }
            }
        }

        return misplacedQueens
    }

    private fun checkDiagonals(): Set<Int> {
        val list = mutableSetOf<Int>()
        list.addAll(this.checkTopLeftBottomRightDiagonal())
        list.addAll(this.checkBottomLeftTopRightDiagonal())
        return list
    }

    private fun checkTopLeftBottomRightDiagonal(): Set<Int> {
        // Every diagonal has the same pattern: the indexes of columns and
        // rows add up to the same number: from 0 up to (this.size - 1) * 2
        // This means we can loop over every couple of indexes, check what
        // their sum equals to and then place the thing into the correspondent
        // index.
        return this.checkDiagonalImplementation { i, j -> i + j }
    }

    private fun checkBottomLeftTopRightDiagonal(): Set<Int> {
        // Every diagonal has the same pattern, but with a twist. If the
        // indexes of the rows are reversed (i.e. 0 becomes this.size - 1,
        // 1 becomes this.size - 2 etc.), the addition between the column
        // index and the row "index" adds up to the same number.
        return this.checkDiagonalImplementation { i, j -> i + this.size - 1 - j }
    }

    private fun checkDiagonalImplementation(sum: (Int, Int) -> Int): Set<Int> {
        // sum -> (queens; columns)
        val map = mutableMapOf<Int, Pair<TriState, MutableSet<Int>>>()

        for (i in 0 until this.size) {
            for (j in 0 until this.size) {
                val s = sum(i, j)

                if (!map.containsKey(s)) map[s] = Pair(TriState(TriState.State.NOT_FOUND), mutableSetOf())
                if (!this.hasQueenAt(BoardIndex(i, j))) continue

                map[s]!!.second.add(i)
                if (map[s]!!.first.state == TriState.State.NOT_FOUND) {
                    map[s]!!.first.state = TriState.State.ONE
                } else if (map[s]!!.first.state == TriState.State.ONE) {
                    map[s]!!.first.state = TriState.State.MORE
                }
            }
        }

        return map.asSequence()
                .map { it.value }
                .filter { it.first.state == TriState.State.MORE }
                .map { it.second }
                .flatten()
                .toSet()
    }
}

private fun buildBoard(positions: IntArray) = Board(positions.size).apply {
    positions.forEachIndexed { i, e -> this.placeQueenAt(BoardIndex(i, e - 1)) }
}

fun qcheck(vararg positions: Int) = buildBoard(positions).findMisplacedQueens().isEmpty()

fun qfix(vararg positions: Int): List<Int> {
    // If the board is already valid there is no need to fix it, right?
    if (qcheck(*positions)) return positions.asList()

    // First things first, we look up the columns where the "problematic"
    // queens are. Then we create a list to store the "new" positions.
    val problematicQueens = buildBoard(positions).findMisplacedQueens()
    val list = mutableListOf<IntArray>()

    // The set contains the column indexes with the queens that are "colliding".
    // We need to swap those with some other position in the index.
    for (i in problematicQueens) {
        for (j in 0 until positions.size) {
            if (i == j) continue
            val baseArray = positions.copyOf()
            val tmp = baseArray[i]
            baseArray[i] = baseArray[j]
            baseArray[j] = tmp
            list.add(baseArray)
        }
    }

    for (array in list) if (qcheck(*array)) return array.asList()
    throw IllegalStateException("Unable to find a solution")
}