package net.thesilkminer.challenges

data class RgbTuple(val r: Int, val g: Int, val b: Int)
private operator fun RgbTuple.plus(that: RgbTuple) = RgbTuple(this.r + that.r, this.g + that.g, this.b + that.b)
private operator fun RgbTuple.div(quotient: Int) = RgbTuple(this.r / quotient, this.g / quotient, this.b / quotient)

private fun Int.toPaddedHex(): String = this.toString(16).padStart(2, '0').toUpperCase()
private fun String.toDecimalInt() = this.toInt(radix = 16)

fun hexColor(r: Int, g: Int, b: Int) = "#${r.toPaddedHex()}${g.toPaddedHex()}${b.toPaddedHex()}"
fun blend(vararg rgbs: String) = rgbs
        .asSequence()
        .map {
            RgbTuple(it.substring(1, 3).toDecimalInt(), it.substring(3, 5).toDecimalInt(), it.substring(5, 7).toDecimalInt())
        }.reduce { sum, element -> sum + element }
        .run { this / rgbs.count() }
        .run {
            hexColor(this.r, this.g, this.b)
        }
