package net.thesilkminer.challenges

import org.junit.Assert
import org.junit.Test

class Challenge369Test {
    @Test
    fun testBasicConversions() {
        Assert.assertEquals("#000000", hexColor(0, 0, 0))
        Assert.assertEquals("#FFFFFF", hexColor(255, 255, 255))
        Assert.assertEquals("#00FF00", hexColor(0, 255, 0))
        Assert.assertEquals("#808000", hexColor(128, 128, 0))
    }

    @Test
    fun testBasicBlends() {
        Assert.assertEquals("#7F7F7F", blend("#000000", "#FFFFFF"))
    }

    @Test
    fun testChallenge() {
        Assert.assertEquals("#FF6347", hexColor(255, 99, 71))
        Assert.assertEquals("#B8860B", hexColor(184, 134, 11))
        Assert.assertEquals("#BDB76B", hexColor(189, 183, 107))
        Assert.assertEquals("#0000CD", hexColor(0, 0, 205))
        Assert.assertEquals("#3B444C", blend("#000000", "#778899")) // Changed test case because it has been wrongly calculated
        Assert.assertEquals("#DCB1D9", blend("#E6E6FA", "#FF69B4", "#B0C4DE"))
    }
}
