package net.thesilkminer.challenges

import org.junit.Assert
import org.junit.Test

class Challenge370Test {
    @Test
    fun testExample() {
        Assert.assertEquals(2, upc(3600029145))
        Assert.assertEquals(2, upc("03600029145"))
    }

    @Test(expected = IllegalArgumentException::class)
    fun testIllegalUpc() {
        upc("0123456789")
    }

    @Test
    fun testChallenge() {
        Assert.assertEquals(4, upc(4210000526))
        Assert.assertEquals(2, upc(3600029145))
        Assert.assertEquals(4, upc(12345678910))
        Assert.assertEquals(0, upc(1234567))
    }
}
