package net.thesilkminer.challenges

import org.junit.Assert
import org.junit.Test

class Challenge372Test {

    @Test
    fun testBasic() {
        // Well, I couldn't come up with different test cases that weren't already considered, so...
        Assert.assertTrue(balanced("xy"))
        Assert.assertFalse(balanced("xxy"))
    }

    @Test
    fun testChallenge() {
        Assert.assertTrue(balanced("xxxyyy"))
        Assert.assertTrue(balanced("yyyxxx"))
        Assert.assertFalse(balanced("xxxyyyy"))
        Assert.assertTrue(balanced("yyxyxxyxxyyyyxxxyxyx"))
        Assert.assertFalse(balanced("xyxxxxyyyxyxxyxxyy"))
        Assert.assertTrue(balanced(""))
        Assert.assertFalse(balanced("x"))
    }

    @Test(expected = IllegalArgumentException::class)
    fun checkInvalidChallenge() {
        balanced("aabbcc")
    }

    @Test
    fun checkValidSingleBonus() {
        Assert.assertTrue(balanced_bonus("x"))
    }

    @Test
    fun testBonus() {
        Assert.assertTrue(balanced_bonus("xxxyyyzzz"))
        Assert.assertTrue(balanced_bonus("abccbaabccba"))
        Assert.assertFalse(balanced_bonus("xxxyyyzzzz"))
        Assert.assertTrue(balanced_bonus("abcdefghijklmnopqrstuvwxyz"))
        Assert.assertFalse(balanced_bonus("pqq"))
        Assert.assertFalse(balanced_bonus("fdedfdeffeddefeeeefddf"))
        Assert.assertTrue(balanced_bonus("www"))
        Assert.assertTrue(balanced_bonus(""))
    }
}
