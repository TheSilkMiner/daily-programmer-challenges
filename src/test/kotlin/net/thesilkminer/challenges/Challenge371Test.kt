package net.thesilkminer.challenges

import org.junit.Assert
import org.junit.Test

class Challenge371Test {

    @Test
    fun testExample() {
        Assert.assertTrue(qcheck(2, 4, 6, 1, 3, 5))
    }

    @Test
    fun testWrongForSure() {
        Assert.assertFalse(qcheck(1, 2, 3, 4, 5, 6, 7, 8))
        Assert.assertFalse(qcheck(1, 1, 1, 1, 1, 1, 1, 1))
    }

    @Test
    fun testChallenge() {
        Assert.assertTrue(qcheck(4, 2, 7, 3, 6, 8, 5, 1))
        Assert.assertTrue(qcheck(2, 5, 7, 4, 1, 8, 6, 3))
        Assert.assertFalse(qcheck(5, 3, 1, 4, 2, 8, 6, 3))
        Assert.assertFalse(qcheck(5, 8, 2, 4, 7, 1, 3, 6))
        Assert.assertFalse(qcheck(4, 3, 1, 8, 1, 3, 5, 2))
    }

    @Test
    fun testBonusNoOp() {
        Assert.assertEquals(listOf(4, 2, 7, 3, 6, 8, 5, 1), qfix(4, 2, 7, 3, 6, 8, 5, 1))
    }

    @Test
    fun testBonusChallenge() {
        Assert.assertEquals(listOf(4, 6, 8, 2, 7, 1, 3, 5), qfix(8, 6, 4, 2, 7, 1, 3, 5))
        Assert.assertEquals(listOf(8, 4, 1, 3, 6, 2, 7, 5), qfix(8, 5, 1, 3, 6, 2, 7, 4))
        Assert.assertEquals(listOf(4, 6, 8, 3, 1, 7, 5, 2), qfix(4, 6, 8, 3, 1, 2, 5, 7))
        Assert.assertEquals(listOf(7, 3, 1, 6, 8, 5, 2, 4), qfix(7, 1, 3, 6, 8, 5, 2, 4))
    }

    @Test(expected = IllegalStateException::class)
    fun testUnsolvableBonusChallenge() {
        // And by unsolvable I mean due to the parameters of this challenge
        qfix(1, 1, 1, 1, 1, 1, 1, 1)
    }
}